import get from './get.js';
import isOutOfBounds from './isOutOfBounds.js';

export default function <T>(
  a: T[][],
  x: number,
  y: number,
  width: number,
  height: number,
  callback: (value: T, x: number, y: number) => void,
  stepX = 1,
  stepY = 1
) {
  for (let i = 0; i < width; i += stepX) {
    for (let j = 0; j < height; j += stepY) {
      const gridX = x + i;
      const gridY = y + j;

      if (isOutOfBounds(a, gridX, gridY)) {
        continue;
      }

      callback(get(a, gridX, gridY), gridX, gridY);
    }
  }
}
