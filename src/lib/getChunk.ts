import create from './create.js';
import forArea from './forArea.js';
import set from './set.js';

export default function <T>(
  a: T[][],
  x: number,
  y: number,
  w: number,
  h: number
) {
  const chunk: unknown[][] = create(w, h, null);
  forArea(a, x, y, w, h, (value, i, j) => set(chunk, i - x, j - y, value));
  return chunk as T[][];
}
