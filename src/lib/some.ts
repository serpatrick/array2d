export default function <T>(
  a: T[][],
  callback: (value: T, x: number, y: number) => boolean
) {
  return a.some((b, y) => b.some((v, x) => callback(v, x, y)));
}
