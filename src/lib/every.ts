export default function <T>(
  a: T[][],
  callback: (value: T, x: number, y: number) => boolean
) {
  return a.every((b, y) => b.every((v, x) => callback(v, x, y)));
}
