import getChunk from './getChunk.js';
import height from './height.js';
import width from './width.js';

export default function <T>(
  a: T[][],
  w: number,
  h: number,
  callback: (chunk: T[][], x: number, y: number) => void
) {
  for (let x = 0; x < width(a); x += w) {
    for (let y = 0; y < height(a); y += h) {
      callback(getChunk(a, x, y, w, h), x, y);
    }
  }
}
