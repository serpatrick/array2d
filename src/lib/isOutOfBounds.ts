import height from './height.js';
import width from './width.js';

export default function <T>(a: T[][], x: number, y: number) {
  return x < 0 || x >= width(a) || y < 0 || y >= height(a);
}
