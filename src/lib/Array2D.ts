import create from './create.js';
import get from './get.js';
import getChunk from './getChunk.js';
import set from './set.js';
import width from './width.js';
import height from './height.js';
import forArea from './forArea.js';
import forEach from './forEach.js';
import forEachChunk from './forEachChunk.js';
import map from './map.js';
import some from './some.js';
import every from './every.js';
import apply from './apply.js';
import isOutOfBounds from './isOutOfBounds.js';

export default class Array2D<T> {
  grid: T[][];

  constructor(grid: T[][]) {
    this.grid = grid;
  }

  static create<T>(width: number, height: number, value: T) {
    const grid: T[][] = create(width, height, value);
    return new Array2D(grid);
  }

  get(x: number, y: number) {
    return get(this.grid, x, y);
  }

  getChunk(x: number, y: number, width: number, height: number) {
    return new Array2D(getChunk(this.grid, x, y, width, height));
  }

  set(x: number, y: number, value: T) {
    set(this.grid, x, y, value);
  }

  forEach(
    callback: (value: T, x: number, y: number) => void,
    stepX?: number,
    stepY?: number
  ) {
    forEach(this.grid, callback, stepX, stepY);
  }

  forArea(
    x: number,
    y: number,
    width: number,
    height: number,
    callback: (value: T, x: number, y: number) => void,
    stepX?: number,
    stepY?: number
  ) {
    forArea(this.grid, x, y, width, height, callback, stepX, stepY);
  }

  forEachChunk(
    width: number,
    height: number,
    callback: (chunk: Array2D<T>, x: number, y: number) => void
  ) {
    forEachChunk(this.grid, width, height, (chunk, x, y) =>
      callback(new Array2D(chunk), x, y)
    );
  }

  map<K>(callback: (value: T, x: number, y: number) => K) {
    const grid = map(this.grid, callback);
    return new Array2D(grid);
  }

  some(callback: (value: T, x: number, y: number) => boolean) {
    return some(this.grid, callback);
  }

  every(callback: (value: T, x: number, y: number) => boolean) {
    return every(this.grid, callback);
  }

  apply(
    grid: T[][],
    x: number,
    y: number,
    callback: (valueA: T, valueB: T) => T
  ) {
    apply(this.grid, grid, x, y, callback);
  }

  isOutOfBounds(x: number, y: number) {
    return isOutOfBounds(this.grid, x, y);
  }

  get width() {
    return width(this.grid);
  }

  get height() {
    return height(this.grid);
  }
}
